
// Assignment 3 - Pointers
// <Your Name>


#include <conio.h>
#include <iostream>

using namespace std;

// TODO: Implement the "SwapIntegers" function

void SwapIntegers(int *pFirst, int *pSecond) {
	int temp = 0;
	temp = *pFirst;
	*pFirst = *pSecond;
	*pSecond = temp;

}

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	_getch();
	return 0;
}
